#include "image.h"

struct image rotate(struct image const* picture);
void image_destroy(struct image* img);
struct image image_create(uint32_t width, uint32_t height);

