#include <stdint.h>
#include <stdio.h>

struct pixel {
    uint8_t b, g, r;
};

#pragma once
struct image {
    uint32_t width, height;
    struct pixel* pixels;
};
