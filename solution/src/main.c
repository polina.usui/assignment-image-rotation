#include "bmp_io.h"
#include "file_io.h"
#include "rotate_image.h"

#include <malloc.h>
#include <stdio.h>

void usage(){
    printf("Usage: ./image-transformer <source-image> <transformed-image>");
}

int main(int args, char **argument) {

    if (args != 3) {
        usage();
        return 1;
    }

    char *file_input = argument[1];
    char *file_output = argument[2];
    FILE* orig_file = NULL;
    FILE* res_file = NULL;


    if (!open_file(&orig_file, file_input, "rb") || !open_file(&res_file, file_output, "wb")) {
        printf("Can't open the file.");
        return 1;
    }

    struct image old_image = {0};
    if (from_bmp(orig_file, &old_image) != READ_OK) {
        printf("Can't open file for read.");
        return 1;
    }

    
    struct image new_image = rotate(&old_image);
    image_destroy(&old_image);
    if (to_bmp(res_file, &new_image) != WRITE_OK) {
        printf("Can't open file for write.");
        return 1;
    }

    if (!close_file(&orig_file) || !close_file(&res_file)) {
        printf("Can't close the files.");
        return 1;
    }

    /* освобождение памяти */
    //free(old_image.pixels);
    //free(new_image.pixels);
    image_destroy(&new_image);
    return 0;
}

