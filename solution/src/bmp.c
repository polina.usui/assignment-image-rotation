#include "bmp_io.h"
#include "rotate_image.h"

#include <assert.h>
#include <malloc.h>
#include <stdio.h>

#define BMP_TYPE 0x4D42
#define BMP_PLANES 1
//#define BMP_HEADER_SIZE 40
#define BITMAPINFOHEADER_SIZE 40

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static uint8_t get_padding(uint32_t width) {
    return width % 4;
}

static struct bmp_header create_header( size_t width, size_t height) {
    return (struct bmp_header) {
            .bfType = BMP_TYPE,
            .bfileSize = (width*height*sizeof(struct pixel)),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BITMAPINFOHEADER_SIZE,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = BMP_PLANES,
            .biBitCount = 24,
            .biCompression = BI_RGB,
            .biSizeImage = (sizeof(struct pixel) * width + get_padding(width)) * height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

static enum read_status read_header(FILE* in, struct bmp_header* header) {
    fseek(in, 0, SEEK_END);
    size_t size = ftell(in);
    if (size < sizeof(struct bmp_header)) {
        return READ_INVALID_HEADER;
    }

    rewind(in);
    fread(header, sizeof(struct bmp_header), 1, in);
    return READ_OK;
}



static enum read_status read_pixels(struct image* img, FILE* in, uint8_t padding) {
    size_t width = img->width;
    size_t height = img->height;
    struct pixel* pixels = malloc(width * height * sizeof (struct pixel));
    //for (size_t i = 0; i < height; i++) {
      //  fread(pixels + i * width, sizeof(struct pixel), width, in);
      //  fseek(in, padding, SEEK_CUR);
    //}

    for (size_t i = 0; i < height; i++) {
    size_t num_of_read_pixels = fread(pixels + i * width, sizeof(struct pixel), width, in);

    if (num_of_read_pixels < width) {
        image_destroy(img);
        printf("Pixels read less than needed\n");
        return READ_INVALID_BITS;
    }

    if (fseek( in, padding, SEEK_CUR)) {
        image_destroy(img);
        printf( "Can't set file pointer to the next row.\n" );
        return READ_INVALID_BITS;
    }
}
   


    img->pixels = pixels;
    return READ_OK;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header = {0};
    if (read_header(in, &header) == READ_INVALID_HEADER) {
        return READ_INVALID_SIGNATURE;
    }
    img->width = header.biWidth;
    img->height = header.biHeight;
    if (fseek( in, header.bOffBits, SEEK_SET )) return READ_INVALID_BITS;
    return read_pixels(img, in, get_padding(header.biWidth));
}



enum write_status write_image(struct image const *img, FILE *out, uint8_t padding) {
    const size_t width = img->width;
    const size_t height = img->height;
    uint64_t zero = 0;
    //uint64_t cnt = 0;
    for (size_t i = 0; i < height; ++i) {
        if (fwrite(img->pixels + i * width, sizeof (struct pixel), width, out) < width) return WRITE_ERROR;

            //cnt += num_of_recorded;
        if (fwrite(&zero, 1, padding, out) < padding) return WRITE_ERROR;

    }


//    if (cnt == height * width) {
//        return WRITE_OK;
//    }
    return WRITE_OK;
}



enum write_status to_bmp(FILE* out, struct image* img) {
    struct bmp_header header = create_header(img->width, img->height);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }
    uint8_t padding = get_padding(header.biWidth);
    return write_image(img, out, padding);
}

