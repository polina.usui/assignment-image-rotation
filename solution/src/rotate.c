#include "rotate_image.h"

#include <malloc.h>
struct image image_create(uint32_t width, uint32_t height) {
    return (struct image) {
            .width = height,
            .height = width,
            .pixels = malloc(sizeof(struct pixel) * width * height)
    };
}

void image_destroy(struct image* img) {
    free(img->pixels);
}

struct image rotate(struct image const* picture) {
    struct image new_picture = image_create(picture->width, picture->height);
    for (uint32_t h = 0; h < picture->width; h++) {
        for (uint32_t w = 0; w < picture->height; w++) {
	    new_picture.pixels[h * new_picture.width + w] = (picture->pixels[(picture->height - 1 - w) * picture->width + h]);
        }
    }

    return new_picture;
}

